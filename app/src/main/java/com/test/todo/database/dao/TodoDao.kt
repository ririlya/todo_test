package com.test.todo.database.dao

import androidx.room.*
import com.test.todo.database.entity.TodoEntity


@Dao
interface TodoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllTodo(todoList: List<TodoEntity>)

    @Query("Select * from Todos")
    suspend fun getAllTodos(): List<TodoEntity>

    @Query("Select * from Todos where id = :id")
    suspend fun getTodoDetail(id: Int): TodoEntity

    @Delete()
    suspend fun deleteTodo(todoEntity: TodoEntity)

    @Query("Delete from Todos")
    suspend fun deleteAllTodo()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTodo(todoEntity: TodoEntity)

    @Query("UPDATE Todos SET title = :title, completed= :status WHERE id =:id")
    fun updateTodo( title: String,status:Boolean, id: String)

}