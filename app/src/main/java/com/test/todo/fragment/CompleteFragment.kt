package com.test.todo.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.test.todo.AddTodoActivity
import com.test.todo.R
import com.test.todo.TodoMainFragment
import com.test.todo.adapter.TodoListAdapter
import com.test.todo.database.entity.TodoEntity
import com.test.todo.databinding.CompleteFragmentBinding
import com.test.todo.utilities.UtilitiesUtil
import java.util.concurrent.Callable

private const val completeListParam = "complete_list"

class CompleteFragment : Fragment() {

    private var _binding: CompleteFragmentBinding? = null
    private val binding get() = _binding!!
    private var completeListTask: ArrayList<TodoEntity> = arrayListOf()
    private lateinit var adapter: TodoListAdapter
    var callback : InterfaceCompleteFragment? = null

    interface InterfaceCompleteFragment{
        fun onDeleteCompleteTask(todoEntity: TodoEntity)
        fun onClickTodo(todoEntity: TodoEntity)
    }

    companion object {
        fun newInstance(completeListTask: String, callback: InterfaceCompleteFragment) = CompleteFragment().apply {
            this.callback = callback
            arguments = Bundle().apply {
                putString(completeListParam, completeListTask)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            val temp = it.getString(completeListParam)
            val itemType = object : TypeToken<ArrayList<TodoEntity>>() {}.type
            completeListTask = Gson().fromJson<ArrayList<TodoEntity>>(temp, itemType)
        }
        arguments?.clear()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = CompleteFragmentBinding.inflate(inflater, container, false)

        binding.svComplete.setOnQueryTextListener(object : android.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                // inside on query text change method we are
                // calling a method to filter our recycler view.
                filter(newText!!)
                return false
            }
        })

        binding.rvPending.setHasFixedSize(true)
        binding.rvPending.layoutManager = LinearLayoutManager(context)
        adapter = TodoListAdapter(completeListTask,object : TodoListAdapter.AddOnClickListener{
            override fun onClickView(position: Int) {
                callback?.onClickTodo(completeListTask[position])
            }

            override fun onDelete(position: Int) {
                UtilitiesUtil.displayMsgSubmit(
                    requireContext(),
                    requireActivity(),
                    "Delete Task?",
                    "Are you sure to delete task? This can't be undone once you press OK.",
                    resources.getString(R.string.ok),
                    Callable<Void?> {
                        callback?.onDeleteCompleteTask(completeListTask[position])
                        null
                    })
            }
        })
        binding.rvPending.adapter = adapter

        return binding.root
    }

    private fun filter(text: String) {
        // creating a new array list to filter our data.
        val filteredlist: ArrayList<TodoEntity> = ArrayList()

        // running a for loop to compare elements.
        for (item in completeListTask) {
            // checking if the entered string matched with any item of our recycler view.
            if (item.todoTitle.toLowerCase().contains(text.toLowerCase())) {
                // if the item is matched we are
                // adding it to our filtered list.
                filteredlist.add(item)
            }
        }
        if (filteredlist.isEmpty()) {
            // if no item is added in filtered list we are
            // displaying a toast message as no data found.
//            Toast.makeText(this, "No Data Found..", Toast.LENGTH_SHORT).show()
        } else {
            // at last we are passing that filtered
            // list to our adapter class.
            adapter.filterList(filteredlist)
        }
    }


}