package com.test.todo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.test.todo.fragment.TodoEditAddFragment

class AddTodoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_todo)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.title = when(intent.getStringExtra("todoEntity").isNullOrEmpty()){
            true-> "Add Task"
            else-> "Edit Task"
        }
        if (savedInstanceState == null) {

            supportFragmentManager.beginTransaction()
                .replace(R.id.container, TodoEditAddFragment.newInstance(intent.getStringExtra("todoEntity")!!))
                .commitNow()
        }
    }

    // don't forget click listener for back button
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}