package com.test.todo.koin

import com.test.todo.viewmodel.TodoViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

var viewModelModule = module {
    viewModel{ TodoViewModel(get()) }
}