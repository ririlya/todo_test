package com.test.todo.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.test.todo.database.entity.TodoEntity
import com.test.todo.repository.TodoRepository
import com.test.todo.response.QueryResponse
import kotlinx.coroutines.launch
import java.util.*
import kotlin.collections.ArrayList

class TodoViewModel(var todoRepository: TodoRepository) : ViewModel() {

    interface TodoCallInterface{
        fun onLoading()
        fun onSuccess(listTodoEntity: ArrayList<TodoEntity>)
        fun onFailed(message: String?)
    }

    interface TodoAddUpdateInterface{
        fun onLoading()
        fun onSuccess()
        fun onFailed(message: String?)
    }

    val isLoading: MutableLiveData<Boolean> = MutableLiveData(true)


    fun getAllTodo(callback:TodoCallInterface){
        viewModelScope.launch {
            callback.onLoading()

            val result = todoRepository.checkTodoExistDb()
            if(result.isEmpty()){
                getTodoApi(callback)
            }
            else{
                callback.onSuccess(result)
                getTodoApi(callback)
            }
        }
    }

    fun getTodoApi(callback:TodoCallInterface){
        viewModelScope.launch {
            val getTodo = todoRepository.fetchAllTodoApi()

            if(getTodo.status == QueryResponse.Status.SUCCESS){
                //do show list
                val listTodoEntity = arrayListOf<TodoEntity>()
                val currentListResponse = getTodo.data
                currentListResponse?.forEach { currentTodo->
                    listTodoEntity.add(
                        TodoEntity(
                            currentTodo.todoId,
                            currentTodo.todoTitle,
                            currentTodo.todoStatus
                        )
                    )
                }

                todoRepository.insertAllTodo(listTodoEntity)
                callback.onSuccess(listTodoEntity)

            }
            else{
                //show error
                callback.onFailed("Please Connect to internet for the first time")
            }

        }
    }

    fun deleteTodo(todo: TodoEntity, callback: TodoCallInterface){
        viewModelScope.launch {
            callback.onLoading()
            val result = todoRepository.deleteTodoApi(todo.todoId)
            if(result.status == QueryResponse.Status.SUCCESS){
                todoRepository.deleteTodo(todo)
                getAllTodo(callback);
            }
            else{
                callback.onFailed(result.message)
            }
        }
    }


    fun addTodo(todo: TodoEntity, callback: TodoAddUpdateInterface){
        viewModelScope.launch {
            callback.onLoading()
            val result = todoRepository.addTodoApi(todo)

            if(result.status == QueryResponse.Status.SUCCESS){
                val currentResponse = result.data
                todoRepository.addTodo(TodoEntity(currentResponse!!.todoId, currentResponse.todoTitle,currentResponse.todoStatus))
                callback.onSuccess()

            }
            else{
                //show error
                callback.onFailed(result.message)
            }
        }
    }

    fun updateTodo(todo: TodoEntity, callback: TodoAddUpdateInterface){
        viewModelScope.launch {
            callback.onLoading()
            val result = todoRepository.updateTodoApi(todo)

            if(result.status == QueryResponse.Status.SUCCESS){
                val currentResponse = result.data
//                todoRepository.updateTodo(TodoEntity(currentResponse!!.todoId, currentResponse.todoTitle,currentResponse.todoStatus))
                callback.onSuccess()

            }
            else{
                //show error
                callback.onFailed(result.message)
            }
        }
    }

}