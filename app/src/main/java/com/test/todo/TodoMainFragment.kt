package com.test.todo

import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.tabs.TabLayoutMediator
import com.google.gson.Gson
import com.test.todo.adapter.ViewPagerAdapter
import com.test.todo.database.entity.TodoEntity
import com.test.todo.databinding.TodoMainFragmentBinding
import com.test.todo.fragment.CompleteFragment
import com.test.todo.fragment.PendingFragment
import com.test.todo.utilities.UtilitiesUtil
import com.test.todo.viewmodel.TodoViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.concurrent.Callable

class TodoMainFragment : Fragment(),CompleteFragment.InterfaceCompleteFragment,PendingFragment.InterfacePendingFragment {

    private var _binding: TodoMainFragmentBinding? = null
    private val binding get() = _binding!!
    private var listString: ArrayList<String> = arrayListOf("Pending Todo","Completed Todo")
    private lateinit var listFragment: ArrayList<Fragment>
    private lateinit var adapter: ViewPagerAdapter

    private val todoViewModel: TodoViewModel by viewModel()

    companion object {
        fun newInstance() = TodoMainFragment()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true);
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
        inflater.inflate(com.test.todo.R.menu.todo_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            com.test.todo.R.id.action_todo_add ->{
                val intent = Intent(context, AddTodoActivity::class.java)
                intent.putExtra("todoEntity", "")
                context?.startActivity(intent)
                return true
            }

        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = TodoMainFragmentBinding.inflate(inflater, container, false)

        listFragment = arrayListOf(PendingFragment.newInstance("",this),CompleteFragment.newInstance("",this))

        adapter = ViewPagerAdapter(this.activity as AppCompatActivity,listFragment)
        binding.viewPager.adapter = adapter
        TabLayoutMediator(binding.tabs, binding.viewPager) { tab, position ->
            tab.text = listString[position]
        }.attach()


        todoViewModel.getAllTodo(object: TodoViewModel.TodoCallInterface{
            override fun onLoading() {
                binding.loading.visibility = View.VISIBLE
            }

            override fun onSuccess(listTodoEntity: ArrayList<TodoEntity>) {
                binding.loading.visibility = View.GONE
                binding.llDataTodo.visibility = View.VISIBLE
                showData(listTodoEntity)
            }

            override fun onFailed(message: String?) {

            }

        })

        return binding.root
    }

    fun showData(listTodoEntity: ArrayList<TodoEntity>){
        val pendingList = listTodoEntity.partition { !it.todoStatus }.first
        val completeList = listTodoEntity.partition { it.todoStatus }.first
        listFragment = arrayListOf(PendingFragment.newInstance(Gson().toJson(pendingList),this),CompleteFragment.newInstance(Gson().toJson(completeList),this))
        adapter.filterList(listFragment)
    }

    override fun onDeleteCompleteTask(todoEntity: TodoEntity) {
        onDeleteTodo(todoEntity)
    }

    override fun onClickTodo(todoEntity: TodoEntity) {
        onTodoDetail(todoEntity)
    }

    fun onTodoDetail(todoEntity: TodoEntity){
        val intent = Intent(context, AddTodoActivity::class.java)
        intent.putExtra("todoEntity", Gson().toJson(todoEntity))
        context?.startActivity(intent)
    }

    fun onDeleteTodo(todoEntity: TodoEntity){
        todoViewModel.deleteTodo(todoEntity,object: TodoViewModel.TodoCallInterface{
            override fun onLoading() {
                binding.loading.visibility = View.VISIBLE
            }

            override fun onSuccess(listTodoEntity: ArrayList<TodoEntity>) {
                binding.loading.visibility = View.GONE
                binding.llDataTodo.visibility = View.VISIBLE
                showData(listTodoEntity)
            }

            override fun onFailed(message: String?) {
                binding.loading.visibility = View.GONE
                UtilitiesUtil.displayOneBtnPopup(
                    requireContext(),
                    requireActivity(),
                    "Error!",
                    message,
                    Callable<Void?> {
                        null
                    })
            }

        })
    }

}