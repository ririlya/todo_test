package com.test.todo.response


class QueryResponse<T>(
    val status: Status,
    val data: T?,
    val message: String?
) {

    enum class Status {
        SUCCESS, ERROR, LOADING
    }

    companion object {
        fun <T> success(data: T): QueryResponse<T> {
            return QueryResponse(
                Status.SUCCESS,
                data,
                null
            )
        }

        fun <T> error(msg: String, data: T?): QueryResponse<T?> {
            return QueryResponse(
                Status.ERROR,
                data,
                msg
            )
        }

        fun <T> loading(data: T?): QueryResponse<T?> {
            return QueryResponse(
                Status.LOADING,
                data,
                null
            )
        }
    }

}