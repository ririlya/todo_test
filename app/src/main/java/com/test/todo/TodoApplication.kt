package com.test.todo

import android.app.Application
import com.test.todo.koin.apiModule
import com.test.todo.koin.databaseModule
import com.test.todo.koin.repositoryModule
import com.test.todo.koin.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class TodoApplication:Application() {
    override fun onCreate() {
        super.onCreate()
        if(BuildConfig.DEBUG){
            Timber.plant(Timber.DebugTree())
        }

        startKoin {
            androidContext(this@TodoApplication)
            modules(
                listOf(
//                    appModule,
                    databaseModule,
                    apiModule,
                    viewModelModule,
                    repositoryModule
                )
            )
        }
    }
}