package com.test.todo.adapter

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.test.todo.database.entity.TodoEntity

class ViewPagerAdapter(activity: FragmentActivity, var fragmentList: ArrayList<Fragment>) :
    FragmentStateAdapter(activity) {

    // method for filtering our recyclerview items.
    fun filterList(filterllist: ArrayList<Fragment>) {
        // below line is to add our filtered
        // list in our course array list.
        fragmentList = filterllist
        // below line is to notify our adapter
        // as change in recycler view data.
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return fragmentList.size
    }

    override fun createFragment(position: Int): Fragment {
        return fragmentList[position]
    }
}