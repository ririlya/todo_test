package com.test.todo.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import com.test.todo.MainActivity
import com.test.todo.R
import com.test.todo.database.entity.TodoEntity
import com.test.todo.databinding.CompleteFragmentBinding
import com.test.todo.databinding.TodoEditAddFragmentBinding
import com.test.todo.utilities.UtilitiesUtil
import com.test.todo.viewmodel.TodoViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*
import java.util.concurrent.Callable

class TodoEditAddFragment : Fragment() {

    private var todo: TodoEntity? = null
    private var todoCopy: TodoEntity? = null
    private var isCompleted: Boolean? = null

    private var _binding: TodoEditAddFragmentBinding? = null
    private val binding get() = _binding!!
    private val todoViewModel: TodoViewModel by viewModel()

    companion object {
        @JvmStatic
        fun newInstance(todoEntityString: String) =
            TodoEditAddFragment().apply {
                arguments = Bundle().apply {
                    putString("TodoEntity", todoEntityString)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            if(!it.getString("TodoEntity").isNullOrEmpty()){
                todo = Gson().fromJson(it.getString("TodoEntity"),TodoEntity::class.java)
                todoCopy = todo
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        _binding = TodoEditAddFragmentBinding.inflate(inflater, container, false)

        binding.rdgTodoStatus.setOnCheckedChangeListener { radioGroup, i ->
            isCompleted = i == 0
        }
        if(todo != null){
            binding.etTitle.setText(todo!!.todoTitle)
            binding.rdgTodoStatus.check(binding.rdgTodoStatus.getChildAt(
                when(todo!!.todoStatus){
                    true-> 0
                    else -> 1
                }
            ).id);

        }

        binding.btnSave.setOnClickListener {
            if(binding.etTitle.text.trim().isEmpty()){
                //please enter some title!
                UtilitiesUtil.displayOneBtnPopup(
                    requireContext(),
                    requireActivity(),
                    "Error!",
                    "Please enter some title!",
                    Callable<Void?> {
                        binding.etTitle.requestFocus()
                        null
                    })
                return@setOnClickListener
            }
            else if(binding.rdgTodoStatus.checkedRadioButtonId == -1 || isCompleted == null){
                //please select one of status!
                UtilitiesUtil.displayOneBtnPopup(
                    requireContext(),
                    requireActivity(),
                    "Error!",
                    "please select one of status!",
                    Callable<Void?> {
                        binding.etTitle.requestFocus()
                        null
                    })
                return@setOnClickListener
            }

            if(todo == null){
                //we add new
                val todoNew = TodoEntity(UUID.randomUUID().toString(),binding.etTitle.text.trim().toString(),isCompleted!!)
                todoViewModel.addTodo(todoNew,object : TodoViewModel.TodoAddUpdateInterface{
                    override fun onLoading() {
                        binding.loading.visibility = View.VISIBLE
                    }

                    override fun onSuccess() {
                        binding.loading.visibility = View.GONE
                        UtilitiesUtil.displayOneBtnPopup(
                            requireContext(),
                            requireActivity(),
                            "Success!",
                            "Successfully update the todo",
                            Callable<Void?> {
                                val intent = Intent(context, MainActivity::class.java)
                                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                context?.startActivity(intent)
                                null
                            })

                    }

                    override fun onFailed(message: String?) {
                        binding.loading.visibility = View.GONE
                        UtilitiesUtil.displayOneBtnPopup(
                            requireContext(),
                            requireActivity(),
                            "Error!",
                            message,
                            Callable<Void?> {
                                null
                            })
                    }

                })
            }
            else{
                //we save
                val todoNew = TodoEntity(todo!!.todoId,binding.etTitle.text.trim().toString(),isCompleted!!)
                todoViewModel.updateTodo(todoNew,object : TodoViewModel.TodoAddUpdateInterface{
                    override fun onLoading() {
                        binding.loading.visibility = View.VISIBLE
                    }

                    override fun onSuccess() {
                        binding.loading.visibility = View.GONE
                        UtilitiesUtil.displayOneBtnPopup(
                            requireContext(),
                            requireActivity(),
                            "Success!",
                            "Successfully update the todo",
                            Callable<Void?> {
                                val intent = Intent(context, MainActivity::class.java)
                                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                context?.startActivity(intent)
                                null
                            })

                    }

                    override fun onFailed(message: String?) {
                        binding.loading.visibility = View.GONE
                        UtilitiesUtil.displayOneBtnPopup(
                            requireContext(),
                            requireActivity(),
                            "Error!",
                            message,
                            Callable<Void?> {
                                null
                            })
                    }

                })
            }
        }
        return binding.root
    }

}