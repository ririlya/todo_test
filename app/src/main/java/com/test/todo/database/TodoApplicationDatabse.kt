package com.test.todo.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.test.todo.database.dao.TodoDao
import com.test.todo.database.entity.TodoEntity

@Database(
    entities = [TodoEntity::class], version = 1
)
abstract class TodoApplicationDatabse : RoomDatabase(){

    abstract fun TodoDao(): TodoDao

    companion object {


        @Volatile
        private var INSTANCE: TodoApplicationDatabse? = null

        fun getDatabase(context: Context): TodoApplicationDatabse {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    TodoApplicationDatabse::class.java,
                    "Todo_Test_Databse"
                ).build()
                INSTANCE = instance
                return instance
            }
        }

    }
}