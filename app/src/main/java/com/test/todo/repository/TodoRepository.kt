package com.test.todo.repository

import com.test.todo.TodoApi
import com.test.todo.database.dao.TodoDao
import com.test.todo.database.entity.TodoEntity
import com.test.todo.response.QueryResponse
import com.test.todo.response.TodoResponse

class TodoRepository(
    var service: TodoApi,
    var todoDao:TodoDao
) {

    suspend fun checkTodoExistDb(): ArrayList<TodoEntity> {
        return ArrayList(todoDao.getAllTodos())
    }


    suspend fun fetchAllTodoApi(): QueryResponse<List<TodoResponse>> {
        return try {
            val getAllTodo = service.getAllTodo()
            QueryResponse(QueryResponse.Status.SUCCESS, getAllTodo, null)
        } catch (e: Exception) {
            QueryResponse(QueryResponse.Status.ERROR, null, e.message)

        }
    }

    suspend fun insertAllTodo(list:ArrayList<TodoEntity>){
        todoDao.deleteAllTodo()
        todoDao.insertAllTodo(list)
    }

    suspend fun deleteTodo(todoEntity: TodoEntity){
        todoDao.deleteTodo(todoEntity)
    }

    suspend fun deleteTodoApi(todoId:String):QueryResponse<Boolean> {
        return try {
            service.deleteTodo(todoId)
            QueryResponse(QueryResponse.Status.SUCCESS, true, null)
        } catch (e: Exception) {
            QueryResponse(QueryResponse.Status.ERROR, null, e.message)

        }
    }


    suspend fun addTodoApi(todoEntity: TodoEntity):QueryResponse<TodoResponse>{
        return try {
            val result = service.addTodo(todoEntity.todoStatus,todoEntity.todoTitle)
            QueryResponse(QueryResponse.Status.SUCCESS, result, null)
        } catch (e: Exception) {
            QueryResponse(QueryResponse.Status.ERROR, null, e.message)

        }
    }

    suspend fun addTodo(todoEntity: TodoEntity){
        todoDao.insertTodo(todoEntity)
    }


    suspend fun updateTodoApi(todoEntity: TodoEntity):QueryResponse<TodoResponse>{
        return try {
            val result = service.updateTodo(todoEntity.todoId,todoEntity.todoStatus,todoEntity.todoTitle)
            QueryResponse(QueryResponse.Status.SUCCESS, result, null)
        } catch (e: Exception) {
            QueryResponse(QueryResponse.Status.ERROR, null, e.message)

        }
    }

    fun updateTodo(todoEntity: TodoEntity){
        todoDao.updateTodo(todoEntity.todoTitle,todoEntity.todoStatus,todoEntity.todoId)
    }
}