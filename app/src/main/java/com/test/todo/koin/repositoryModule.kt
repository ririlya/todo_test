package com.test.todo.koin

import com.test.todo.repository.TodoRepository
import org.koin.dsl.module

var repositoryModule = module {

    single { TodoRepository(get(),get()) }
}