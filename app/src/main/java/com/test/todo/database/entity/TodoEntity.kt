package com.test.todo.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Todos")
data class TodoEntity(
    @ColumnInfo(name = "id") @PrimaryKey var todoId: String,
    @ColumnInfo(name = "title")var todoTitle: String,
    @ColumnInfo(name = "completed")var todoStatus: Boolean,
)