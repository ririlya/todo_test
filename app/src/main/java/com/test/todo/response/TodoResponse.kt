package com.test.todo.response

import com.google.gson.annotations.SerializedName

data class TodoResponse(

    @field:SerializedName("id")
    val todoId: String = "",

    @field:SerializedName("title")
    val todoTitle: String = "",

    @field:SerializedName("completed")
    val todoStatus: Boolean = false
)