package com.test.todo.koin

import com.test.todo.BuildConfig
import com.test.todo.TodoApi
import okhttp3.CertificatePinner
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

var apiModule = module {

    single { provideRetrofit() }
    single { providerTodoAPI(get()) }
}

/**
 * create retrofit instant
 */
fun provideRetrofit(): Retrofit {

    val okHttpClient = OkHttpClient.Builder()
        .build()

    return Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}

/**
 * create Api Service instant instant
 */
fun providerTodoAPI(retrofit: Retrofit): TodoApi =
    retrofit.create(TodoApi::class.java)
