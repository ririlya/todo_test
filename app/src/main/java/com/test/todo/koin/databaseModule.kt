package com.test.todo.koin

import android.content.Context
import com.test.todo.database.TodoApplicationDatabse
import com.test.todo.database.dao.TodoDao
import org.koin.dsl.module

var databaseModule = module {
    single { provideDatabase(get()) }
    single { provideTodoDao(get()) }

}
fun provideDatabase(context: Context): TodoApplicationDatabse {
    return TodoApplicationDatabse.getDatabase(context)
}

fun provideTodoDao(database: TodoApplicationDatabse): TodoDao {
    return database.TodoDao()
}