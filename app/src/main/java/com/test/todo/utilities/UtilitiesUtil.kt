package com.test.todo.utilities

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.widget.Button
import android.widget.TextView
import com.test.todo.R
import java.util.concurrent.Callable

object UtilitiesUtil {
    fun displayMsgSubmit(mContext: Context, mActivity: Activity, title:String, msg:String?, confirmMsg:String, funcCall : Callable<Void?>){
        val builder = AlertDialog.Builder(mContext, R.style.Style_Dialog_Rounded_Corner).create()
        builder.setCancelable(false)
        val inflater = mActivity.layoutInflater
        val dialogLayout = inflater.inflate(R.layout.layout_popup_two_btn, null)
        val tvDesc = dialogLayout.findViewById(R.id.tvAccDesc) as TextView
        val tvTitle = dialogLayout.findViewById(R.id.tvTitle) as TextView
        val btnOk = dialogLayout.findViewById(R.id.btnOk) as Button
        val btnCancel = dialogLayout.findViewById(R.id.btnCancel) as Button

        tvTitle.text = title
        tvDesc.text = msg
        btnOk.text = confirmMsg

        btnOk.setOnClickListener {
            funcCall.call()
            builder.dismiss()
        }
        btnCancel.setOnClickListener {
            builder.dismiss()
        }
        builder.setView(dialogLayout)
        builder.show()
    }


    fun displayOneBtnPopup(mContext: Context, mActivity: Activity, title:String, msg:String?,funcCall : Callable<Void?>){
        val builder = AlertDialog.Builder(mContext, R.style.Style_Dialog_Rounded_Corner).create()
        builder.setCancelable(false)
        val inflater = mActivity.layoutInflater
        val dialogLayout = inflater.inflate(R.layout.layout_popup_one_btn, null)
        val tvDesc = dialogLayout.findViewById(R.id.tvAccDesc) as TextView
        val tvTitle = dialogLayout.findViewById(R.id.tvTitle) as TextView
        val btnOk = dialogLayout.findViewById(R.id.btnOk) as Button

        tvTitle.text = title
        tvDesc.text = msg

        btnOk.setOnClickListener {
            funcCall.call()
            builder.dismiss()
        }
        builder.setView(dialogLayout)
        builder.show()
    }
}