package com.test.todo

import com.test.todo.response.TodoResponse
import okhttp3.ResponseBody
import retrofit2.http.*


interface TodoApi {
    @GET("todo")
    suspend fun getAllTodo(): List<TodoResponse>

    @DELETE("todo/{id}")
    suspend fun deleteTodo(@Path("id") todoId: String)

    @FormUrlEncoded
    @POST("todo")
    suspend fun addTodo(@Field("completed") status: Boolean, @Field("title") title: String): TodoResponse

    @FormUrlEncoded
    @PUT("todo/{id}")
    suspend fun updateTodo(@Path("id") todoId: String,@Field("completed") status: Boolean, @Field("title") title: String): TodoResponse

}