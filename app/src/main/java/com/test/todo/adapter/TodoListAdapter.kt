package com.test.todo.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.test.todo.R
import com.test.todo.database.entity.TodoEntity
import com.test.todo.databinding.LayoutTaskBinding
import java.util.ArrayList


class TodoListAdapter(
    recycleItemArrayList: ArrayList<TodoEntity>,
    callback: AddOnClickListener
) : RecyclerView.Adapter<TodoListAdapter.TodoItemViewHolder>() {

    var recycleItemArrayList = ArrayList<TodoEntity>()
    var callback: AddOnClickListener


    init {
        this.recycleItemArrayList = recycleItemArrayList
        this.callback = callback
    }

    interface AddOnClickListener {
        fun onClickView(position: Int)
        fun onDelete(position: Int)
    }

    // method for filtering our recyclerview items.
    fun filterList(filterllist: ArrayList<TodoEntity>) {
        // below line is to add our filtered
        // list in our course array list.
        recycleItemArrayList = filterllist
        // below line is to notify our adapter
        // as change in recycler view data.
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoItemViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.layout_task, parent, false)
        return TodoItemViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: TodoItemViewHolder, position: Int) {
        val todoItemData = recycleItemArrayList[position]

        holder.binding.tvTodoTitle.text = todoItemData.todoTitle
        holder.binding.ivStatus.setImageResource(when(todoItemData.todoStatus){
            true->R.drawable.ic_done
            else-> R.drawable.ic_pending
        })
        holder.binding.llWholeTask.setOnClickListener {
            callback.onClickView(position)
        }
        holder.binding.ivDelete.setOnClickListener {
            callback.onDelete(position)
        }
    }

    override fun getItemCount(): Int {
        return recycleItemArrayList.size
    }

    class TodoItemViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var binding: LayoutTaskBinding = LayoutTaskBinding.bind(itemView)

    }


}